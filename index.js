var express = require('express');
var multer = require('multer');
var fs = require('fs');
var cors = require("cors");
var crypto = require("crypto");
var mime = require("mime");
var path = require('path');

var app = express();

var DIR = './uploads/';

var storage = multer.diskStorage({
        destination: function(req, file, cb) {
                cb(null, DIR)
        },
        filename: function(req, file, cb) {
                crypto.pseudoRandomBytes(16, function(err, raw) {
                        cb(null, raw.toString('hex') + Date.now() + '.' + file.originalname);
                });
        }
});

var upload = multer({ storage: storage });

app.use(cors({ credentials: true, origin: 'http://flo.dev:5000' }));

app.options('/api'); // enable pre-flight request for DELETE request
app.get('/api', function(req, res) {
        res.end('file catcher example');
});

app.post('/api', upload.any(), function(req, res, next) {
        // req.body contains the text fields
        let body = JSON.stringify({
                dealDocumentId: 9,
                createdDate: "2016-05-27T15:45:00",
                createdUserName: "rotten",
                dealDocumentName: "Document 1",
                dealDocumentDescription: null,
                dealId: 7,
                displayOrder: 1,
                dealDocumentCategoryId: 55,
                categoryShortName: "UNCAT",
                categoryName: "Uncatagorized",
                dealDocumentSectionId: 126,
                sectionShortName: "COOV",
                mediaTypeId: 1,
                shortName: "IMAGE",
                documentPath: "x"
        });
        
        res.end(body);
});

var PORT = process.env.PORT || 3000;

app.listen(PORT, function() {
        console.log('Working on port ' + PORT);
});